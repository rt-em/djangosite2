# !/bin/bash

################################################################################
#Установка общих зависимостей проекта
################################################################################

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-setuptools python-dev libpq-dev libxml2-dev build-essential python-virtualenv nginx uwsgi uwsgi-plugin-python postgresql pwgen
sudo easy_install pip


################################################################################
# Создание виртуального окружения python для движка и установка зависимостей 
# django проекта
################################################################################
mkdir ~/env/
virtualenv ~/env/djangosite2 -p python3
. ~/env/djangosite2/bin/activate
pip install -r requirements.txt


################################################################################
# Чтение конфига установки
################################################################################
source variables.cfg

################################################################################
# Создание базы данных проекта
################################################################################
db_pass=$(pwgen -1 -s -n 20)
sudo su postgres -c "
psql template1 <<END
CREATE USER $project_name WITH CREATEDB NOCREATEROLE NOINHERIT NOSUPERUSER REPLICATION;
ALTER ROLE $project_name WITH ENCRYPTED PASSWORD '$db_pass';
\q
END
createdb -O $project_name $project_name
exit
psql template1 <<END
GRANT ALL PRIVILEGES ON DATABASE $project_name TO $project_name;
\q
END
"

################################################################################
# Правка settings.py
################################################################################

secret_key=$(pwgen -1 -s -n 50)
cp ./djangosite2/settings_template/settings.py ./djangosite2/settings.py
sed s#very_secret_key#$secret_key#g ./djangosite2/settings.py --in-place
sed s#project_name#$project_name#g ./djangosite2/settings.py --in-place
sed s#db_pass#$db_pass#g ./djangosite2/settings.py --in-place


################################################################################
# Миграция проекта
################################################################################
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py createsuperuser

################################################################################
# Переименование проекта
################################################################################
if [[ ${PWD##*/} = $project_name ]]
then
echo "Папка проекта уже имеет нужное имя"
else
current_dirname=${PWD##*/}
cd ..
mv $current_dirname $project_name 
echo "Директория переименована"
cd $project_name
fi
mkdir -m 777 media

################################################################################
# Генерация конфигов сервера
################################################################################
cp ./prod_config/nginx/template/djangosite. ./prod_config/nginx/$project_name.
cp ./prod_config/uwsgi/template/djangosite.ini ./prod_config/uwsgi/$project_name.ini
cp ./dev_config/nginx/template/djangosite. ./dev_config/nginx/$project_name.
cp ./dev_config/uwsgi/template/djangosite.ini ./dev_config/uwsgi/$project_name.ini

proj_dir=$(pwd)
sed s#project_name#$project_name#g ./prod_config/nginx/$project_name. --in-place
sed s#project_url#$site_url#g ./prod_config/nginx/$project_name. --in-place
sed s#project_root#$proj_dir#g ./prod_config/nginx/$project_name. --in-place
sed s#project_name#$project_name#g ./dev_config/nginx/$project_name. --in-place
sed s#project_root#$proj_dir#g ./dev_config/nginx/$project_name. --in-place

cd ~ 
home_dir=$(pwd)
echo $home_dir
cd -

sed s#home_dir#$home_dir#g ./prod_config/uwsgi/$project_name.ini --in-place
sed s#proj_dir#$proj_dir#g ./prod_config/uwsgi/$project_name.ini --in-place
sed s#home_dir#$home_dir#g ./dev_config/uwsgi/$project_name.ini --in-place
sed s#proj_dir#$proj_dir#g ./dev_config/uwsgi/$project_name.ini --in-place

################################################################################
# Символические ссылки сервисов
################################################################################

if [[ $production = 1 ]]
then
sudo ln -s $proj_dir/prod_config/nginx/$project_name. /etc/nginx/sites-enabled/$project_name.
sudo ln -s $proj_dir/prod_config/uwsgi/$project_name.ini /etc/uwsgi/apps-enabled/$project_name.ini 
else
sudo ln -s $proj_dir/dev_config/nginx/$project_name. /etc/nginx/sites-enabled/$project_name.
sudo ln -s $proj_dir/dev_config/uwsgi/$project_name.ini /etc/uwsgi/apps-enabled/$project_name.ini
fi

################################################################################
#Рестарт сервисов
################################################################################

sudo service uwsgi restart $project_name
sudo service nginx restart $project_name
