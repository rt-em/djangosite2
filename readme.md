# Установка сборки #

## Операционные системы ##
Сборка протестирована на Ubuntu 18.04. Работа на других системах не гарантирована.

## Общие пололжения ##
Установка сборки ведется под учетной записью простого пользователя.

## Алгоритм установки ##

1.В папке пользователя создать папку proj или любую другую папку куда у вас будет установлена сборка
```
$ mkdir proj
$ cd proj
```
2.Копируем репозиторий в папку proj
```
$ git clone https://rt-em@bitbucket.org/rt-em/djangosite2.git
```
3.Заходим в папку проекта
```
$ cd djangosite2
```
4.Открываем файл variables.cfg и прописываем переменные
```
$ nano variables.cfg
```
5.Запускаем установку
```
$ ./istall.sh
```
6.Смотрим что запрашивает скрипт и вписываем требуемые параметры.
7.Заходим по адресу сайта или по адресу 127.0.0.1:8002 если устанавливали на локальную машину с параметром production=0 и радуемся результату.

## Полезные команды ##
Рестарт сервисов:
```
$ sudo service uwsgi restart
$ sudo service uwsgi restart
```